import { StyleSheet } from 'react-native';
import CarrinhoCheio from './src/pages/CarrinhoCheio/index';
import CarrinhoVazio from './src/pages/CarrinhoVazio/index';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator()
export default function App() {
  return (
  <NavigationContainer> 
    <Stack.Navigator>

      <Stack.Screen name='CarrinhoCheio' component={CarrinhoCheio}/>
      <Stack.Screen name='CarrinhoVazio' component={CarrinhoVazio} options={{headerShown: false}} />

    </Stack.Navigator>
  </NavigationContainer>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

import react from 'react';
import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import { Container, ImageAndText, HelloMessage, BtnLogout } from './style';
interface Props {
    name: string;
    urlImage: any;
}

const Header = ({ name, urlImage }: Props) => {
    return (
        <Container>
            <ImageAndText>
                <Image source={{ uri: urlImage }} style={{ height: 37, width: 45 }} />
                <HelloMessage> olá, {name}!!</HelloMessage>
            </ImageAndText>

            <BtnLogout activeOpacity={0.6}>
                <Entypo name="log-out" size={24} color="#FFF4F4" />
            </BtnLogout>
        </Container>

    );
}
export default Header;

import styled from 'styled-components/native';

export const Container = styled.View`
    flex-direction:row;
    width: 4100px;
    height: 80px;
    background-color: #58BCC7;
    align-items: center;
`
export const ImageAndText = styled.View`
    flex-direction: row;
    align-items: center;
    margin-left: 10px;
`
export const HelloMessage = styled.Text`
    color: #FFF4F4;
    font-weight: 600;
    font-size: 15px;
    margin-left: 10px;
`
export const BtnLogout = styled.TouchableOpacity`
    margin-left: 215px;
`
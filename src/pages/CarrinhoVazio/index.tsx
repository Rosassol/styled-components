import Header from '../Components/Header/index';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View, Text, } from 'react-native';
import { Case, ButtonCompre, TextWarning, TextButton, CartAndClear, Cart, Clear } from './style';

export default function CarrinhoVazio() {
    return (
        <View>
            <Header name={'Natasha'} urlImage={require('../../../assets/perfil.png')} />
            <Case>
                <CartAndClear>
                    <Cart>Meu carrinho</Cart>
                    <Clear>Limpar</Clear>
                </CartAndClear>

                <MaterialCommunityIcons name="cart-outline" size={150} color="rgba(88, 188, 199, 0.53);" />

                <TextWarning>Seu carrinho de compras está vazio</TextWarning>
                <Text>Navege pelas nossas ofertas incríveis agora!</Text>

                <ButtonCompre>
                    <TextButton >Compre Agora</TextButton >
                </ButtonCompre>

            </Case>
        </View>
    );

}  
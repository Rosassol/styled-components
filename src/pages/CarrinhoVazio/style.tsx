import styled from 'styled-components/native';

export const Case = styled.View`
    flex:1;
    justify-content: column;
    align-items: center;
`
export const CartAndClear = styled.View`
    flex-direction:row;
    justify-content: space-between;
    margin-bottom:90px;
`
export const Cart = styled.Text`
    color: #5bbcc7;
    font-weight: 500;
    font-size:30px;
    
`
export const Clear = styled.Text`
    color: #5bbcc7;
    font-weight: 400;
    font-size: 18px;
    margin-left: 120px;
`

export const TextWarning = styled.Text`
    color: black;
    font-weight: 600;
`
export const ButtonCompre = styled.TouchableOpacity`
    margin-top: 10px;
    height: 45px;
    width: 150px;
    background-color: #5bbcc7;
    border-radius: 20px;
`
export const TextButton = styled.Text`
    color: #FFFFFF;
    margin-top: 10px;
    font-size: 15px;
    text-align:center; 
`
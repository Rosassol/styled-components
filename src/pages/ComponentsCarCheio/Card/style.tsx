import styled from 'styled-components/native';

export const Pack = styled.View`
    width: 328px;
    height: 111px;
    background: rgba(88, 188, 199, 0.53);
    border-radius: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;   
`
export const United = styled.Text`
    color: #FFFFFF;
    background-color: #5BBCC7;
    border-radius: 11px;
    width: 50px;
    height:22px ;
    text-align: center;
    margin-top: -22px;
    z-index:1;
`
export const UnitedText = styled.Text`
    color:#000000;
    font-weight: 400;
    font-size: 18px;
`
export const PriceText = styled.Text`
    color:#FFFFFF;
    font-weight: 400;
    font-size: 18px;

`
export const Iconscard = styled.View`
    display: flex;
    flex-direction:row;
    justify-content: space-between;
    align-items: flex-start;
    padding-right: 50%;
    margin-top: 2%;
`
export const LeftView = styled.View`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    width: 60%;
    margin-left: 5%;
`

export const RightView = styled.View`
    margin-right: 5%;
`
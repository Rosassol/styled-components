import { Image } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { United, UnitedText, PriceText, Iconscard, Pack, LeftView, RightView } from './style';

export default function Card() {
    return (
        <Pack>
            <LeftView>
                <UnitedText>1x Biquíni</UnitedText>
                <PriceText>R$ 59,90</PriceText>

                <Iconscard>
                    <AntDesign name="minuscircleo" size={24} color="black" />
                    <AntDesign name="pluscircleo" size={24} color="black" />
                    <Ionicons name="trash" size={24} color="black" />
                </Iconscard>
            </LeftView>

            <RightView>
                <Image source={require('../../../../assets/image-compra.png')} style={{ height: 95, width: 100 }} />
                <United>1 un.</United>
            </RightView>
        </Pack>

    );

}


import Header from '../Components/Header/index';
import { Text, TouchableOpacity, View, } from 'react-native';

import { Carton, CartAndClearC, CartC, ClearC, AdditionText, Pack, PackText, PackText2, ButtonFinish, Btntext, Resume, FreeText, TotalText, Btnprice, FinishText } from './style';

import Card from '../ComponentsCarCheio/Card';
import { useNavigation } from '@react-navigation/native';

export default function CarrinhoCheio() {
    const navigation = useNavigation()
    return (
        <View>
            <Header name={'Natasha'} urlImage={require('../../../assets/perfil.png')} />


            <Carton>
                <CartAndClearC>
                    <CartC>Meu carrinho</CartC>
                    <TouchableOpacity onPress={() => navigation.navigate('CarrinhoVazio')}>
                        <ClearC>Limpar</ClearC>
                    </TouchableOpacity>

                </CartAndClearC>
                <Card />

                <AdditionText>Adicionar mais itens</AdditionText>

                <Pack>
                    <PackText>
                        <Resume>Resumo de valores </Resume>
                        <Text>Sub-Total</Text>
                        <Text>Taxa de entrega</Text>
                    </PackText>

                    <PackText2>
                        <Text>R$ 59,90</Text>
                        <Text>+ CUPOM 15% OFF</Text>
                        <FreeText>GRÁTIS</FreeText>
                    </PackText2>
                </Pack>

                <ButtonFinish>
                    <Btntext>
                        <TotalText>TOTAL</TotalText>
                        <Btnprice>R$50,00</Btnprice>
                    </Btntext>
                    <FinishText>Finalizar compra</FinishText>
                </ButtonFinish>
            </Carton>
        </View>

    );
}
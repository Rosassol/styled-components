import styled from 'styled-components/native';

export const Carton = styled.View`
    flex:1;
    justify-content: column;
    align-items: center;
`
export const CartAndClearC = styled.View`
    flex-direction:row;
    justify-content: space-between;
    margin-bottom:90px;
    `
export const CartC = styled.Text`
    color: #5bbcc7;
    font-weight: 500;
    font-size:30px;
    `
export const ClearC = styled.Text`
    color: #5bbcc7;
    font-weight: 400;
    font-size: 18px;
    margin-left: 120px;
    `
export const AdditionText = styled.Text`
    color: #5BBCC7;
    font-weight: 400;
    font-size: 15px;
    line-height: 18px;
    margin-bottom: 5%;
    margin-top: 2%;
`
export const  Pack  = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-end;
    margin-top: 5px;
`
export const  PackText  = styled.Text`
    color: #5BBCC7;
    font-weight: 400;
    font-size: 15px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
`
export const Resume = styled.Text`
    font-weight: 600;
`
export const  PackText2  = styled.Text`
    color: #5BBCC7;
    font-weight: 400;
    font-size: 15px;
     display: flex;
    flex-direction: column;
    align-items:flex-end;
    margin-left: 10px;
`
export const FreeText = styled.Text`
    font-weight: 600;
`
export const ButtonFinish  = styled.TouchableOpacity`
    width: 330px;
    height: 70px;
    display: flex;
    flex-direction: row;
    margin-top: 50px;
    background: #58BCC7;
    border-radius: 20px;
    justify-content: space-around;
    align-items: center;
`
export const Btntext = styled.Text`
    font-weight: 600;
    font-size: 18px;
    display: flex;
    flex-direction: column;
    text-align: center;
    color: #FFFFFF;
`
export const TotalText = styled.Text`
     font-weight: 600;
`
export const Btnprice = styled.Text`
    font-weight: 400;
    font-size: 14px;
    margin-right: 10px;

`
export const FinishText = styled.Text`
    color: #FFF4F4;
    font-size: 18px;
`